# secure_channel_client

## 安装第三方依赖
```yum install cmake openssl-libs openssl-devel cjson-devel```
## 编译安装鲲鹏安全库
```
git clone https://gitee.com/openeuler/kunpengsecl.git
cd kunpengsecl/attestation/tee/tverlib/miracl
make
cd ../verifier
make
cp libteeverifier.so /usr/lib64
```
## 编译安装安全通道客户端SDK
```
mkdir build && cmake .. && make && make install
```
## 编译运行demo
- 配置安全通道服务端所在TA的基线值到basevalue.txt
- 修改client.c中服务ip和port

```
cd example
mkdir build && cmake .. && make
./sc_client
```